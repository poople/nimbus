## 使用说明

### 项目编译打包

执行`package.sh`脚本。即可在target目录下看到打包好的zip包。

### 启动

解压target/nimbus-1.0-bin.zip. 执行`bin/run.sh`启动服务

### 动态刷新配置文件

执行 `bin/reload_config.sh`即可.



