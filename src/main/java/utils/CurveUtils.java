package utils;

import lombok.extern.slf4j.Slf4j;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by peter on 16/6/27.
 */
@Slf4j
public class CurveUtils {

    private static AtomicInteger total1min = new AtomicInteger(0);
    public static AtomicInteger total5Min = new AtomicInteger(0);
    public static AtomicInteger total5MinRest = new AtomicInteger(0);


    public static int getTotalNumber(long now) throws SQLException {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(now);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        int i = hour*12 + minute/5 + 1;

        String sql = "select slice_val from hm_curve_discret where slice_no = " + i +" and deleted is null and curve_id in ( " +
                "    select id from hm_curve_sele where deleted is null and name='" + ConfigUtil.getString("curve.name") +"' " +
                " )";
        log.info("sql={}", sql);
        int total = 0;
        total = (int) DBUtils.getObject(sql);

        double coeffix = getCoeffix();
        double randEnhance = Math.random() * ConfigUtil.getInt("curve.amp.max") / 100;
        return (int)(total * (coeffix + randEnhance));
    }

    private static double getCoeffix() throws SQLException {
        String curveName = ConfigUtil.getString("curve.name");
        Double taskSum = Double.valueOf(ConfigUtil.getInt("hammer.taskSum"));
        String sqlComm = "select base_count from hm_curve_sele where deleted is null and name='" + curveName+ "'";
        int baseCount = (int) DBUtils.getObject(sqlComm);
        return taskSum /baseCount;
    }

    public static int get(){
        return total1min.get();
    }

    public static void set(int value){
        total1min.set(value);
    }

    public static int decrementAndGet(){
        total5MinRest.decrementAndGet();
        return total1min.decrementAndGet();
    }

    public static void setTotal(int value){
        total5MinRest.set(value);
        total5Min.set(value);
    }

    public static int getTotal(){
        return total5Min.get();
    }

    public static int getTotalRest(){
        return total5MinRest.get();
    }

    public static int getTaskNumber(int total, int totalRest){
        double percent = RandomUtils.getRandomNumber(15, 30)/100.0;

        Calendar cal = Calendar.getInstance();
        int min = cal.get(Calendar.MINUTE);
        int rest = min % 5;

        if(totalRest <= 0){
            return 0;
        }

        if(rest != 4){
            return (int) (percent * total);
        }else{
            return totalRest;
        }
    }

    public static void main(String[] args) {
        int total = 48;
        int rest = 0;
        System.out.println(getTaskNumber(total, rest));
        System.out.println(getTaskNumber(total, rest));
        System.out.println(getTaskNumber(total, rest));
        System.out.println(getTaskNumber(total, rest));
    }
}
