package utils;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Created by peter on 16/6/5.
 */
@Slf4j
public class RedisUtils {

    private static JedisPool pool = null;

    static {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(20);
        config.setMaxIdle(10);
        config.setMinIdle(3);
        config.setMaxWaitMillis(3000);
        config.setTestOnBorrow(true);
        config.setLifo(true);
        config.setTimeBetweenEvictionRunsMillis(1000);
        config.setTestOnReturn(true);
        config.setTestWhileIdle(true);

        pool = new JedisPool(config, ConfigUtil.getString("redis.server"), ConfigUtil.getInt("redis.port"));
    }

    private static Jedis getRedis() {
        return pool.getResource();
    }

    public static void incr(String key) {
        key = CommonUtils.withMode(key);
        Jedis redis = null;
        try {
            redis = getRedis();
            redis.expireAt(key, CommonUtils.getExpireTime());
            redis.incr(key);
        } finally {
            redis.close();
        }
    }

    public static Long llen(String key){
        key = CommonUtils.withMode(key);
        Jedis redis = null;
        try {
            redis = getRedis();
            return redis.llen(key);
        } finally {
            redis.close();
        }
    }

    public static int getValueInt(String key) {
        key = CommonUtils.withMode(key);
        Jedis redis = null;
        try {
            redis = getRedis();
            if (redis.get(key) != null) {
                return Integer.parseInt(redis.get(key));
            } else {
                return 0;
            }
        } finally {
            redis.close();
        }
    }

    public static void lpush(String key, String value) {
        key = CommonUtils.withMode(key);
        Jedis redis = null;
        try {
            redis = getRedis();
            redis.lpush(key, value);
        } finally {
            redis.close();
        }
    }

    public static String rpop(String key) {
        key = CommonUtils.withMode(key);
        Jedis redis = null;
        try {
            redis = getRedis();
            return redis.rpop(key);
        } finally {
            redis.close();
        }
    }

    public static long getValueLong(String key) {
        key = CommonUtils.withMode(key);
        Jedis redis = null;
        try {
            redis = getRedis();
            if (redis.get(key) != null) {
                return Long.parseLong(redis.get(key));
            } else {
                return 0;
            }
        } finally {
            redis.close();
        }
    }

    public static void setLongValue(String key, Long value) {
        key = CommonUtils.withMode(key);
        Jedis redis = null;
        try {
            redis = getRedis();
            redis.set(key, String.valueOf(value));
        } finally {
            redis.close();
        }
    }

    public static String keyWithCount(String key, String tn) {

        return key + "_" + tn + "_count";
    }


    public static String keyWithLastSendTime(String key) {
        return key + "_lastSendTime";
    }


}
