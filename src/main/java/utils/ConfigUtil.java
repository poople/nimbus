package utils;

import lombok.extern.slf4j.Slf4j;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by peter on 16/7/6.
 */
@Slf4j
public class ConfigUtil {

    private static Properties config = new Properties();


    static {
        loadConfig();
    }

    public static void loadConfig(){
        String confFile = System.getProperty("conf.file");
        try {
            log.info("#### Initializing config file {}", confFile);
            config.load(new FileReader(confFile));
        } catch (IOException e) {
            log.info("init config properties exception.", e);
        }
    }

    public static Object get(String key){
        return config.get(key);
    }

    public static String getString(String key){
        return (String) config.get(key);
    }

    public static Integer getInt(String key){
        String value = (String) config.get(key);
        return value == null? -1: Integer.parseInt(value);
    }

    public static Boolean getBoolean(String key){
        String value = (String) config.get(key);
        return (value != null && value.trim().equals("true")) ? true: false;
    }
}
