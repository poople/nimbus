package utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.*;

import java.util.Arrays;
import java.util.Properties;

/**
 * Created by peter on 16/6/26.
 */
@Slf4j
public class KafkaUtils {

    private static Producer producer = null;

    static {
        String server = ConfigUtil.getString("kafka.server");
        producer = createProducer(server);
    }

    public static void send(String topic, String key, String value){
        producer.send(new ProducerRecord<>(topic, key, value));
    }

    public static void send(String topic, String key, String value, Callback callback){
        producer.send(new ProducerRecord<>(topic, key, value), callback);
    }

    private static Producer createProducer(String server){
        Properties props = new Properties();

        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, server);
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.RETRIES_CONFIG, 10);
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
        props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        return new KafkaProducer(props);
    }


}
