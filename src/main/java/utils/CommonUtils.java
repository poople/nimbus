package utils;

import Model.HammerTask;
import lombok.extern.slf4j.Slf4j;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

/**
 * Created by peter on 16/7/3.
 */
@Slf4j
public class CommonUtils {

    public static HammerTask HAMMER_TASK = null;
    public static String mode = "";
    private static long EXPIRE_TIME = Integer.MAX_VALUE;

    static{
        try {
            HAMMER_TASK = getTaskInfo();
            mode = getMode();
        } catch (SQLException e) {
            log.error("get hammer task info error! id=" + ConfigUtil.getString("hammer.taskId"), e);
        }
    }

    private static String getMode(){
        boolean isDryRunMode = ConfigUtil.getBoolean("nimbus.dryRun.mode");
        if(isDryRunMode){
            return "_dryRun";
        }else{
            return "";
        }
    }

    public static String withMode(String str){
        return str + mode;
    }

    public static HammerTask getTaskInfo() throws SQLException {
        String taskId = ConfigUtil.getString("hammer.taskId");
        List<HammerTask> result = DBUtils.getObjects("select * from hm_task_info where id=" + taskId, HammerTask.class);
        return result==null? null : result.get(0);
    }

    public static void setExpireTime(long expire_time){
        EXPIRE_TIME = expire_time;
    }

    public static long getExpireTime(){
        return EXPIRE_TIME;
    }
}
