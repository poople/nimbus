package utils;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.beans.PropertyVetoException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by peter on 16/6/5.
 */
@Slf4j
public class DBUtils {

    private static ComboPooledDataSource cpds = null;

    static {
        cpds = new ComboPooledDataSource();
        try {
            cpds.setDriverClass(ConfigUtil.getString("jdbc.driver")); //loads the jdbc driver

            cpds.setJdbcUrl(ConfigUtil.getString("jdbc.url"));
            cpds.setUser(ConfigUtil.getString("jdbc.user"));
            cpds.setPassword(ConfigUtil.getString("jdbc.passwd"));

            cpds.setMinPoolSize(5);
            cpds.setAcquireIncrement(5);
            cpds.setMaxPoolSize(20);
            cpds.setPreferredTestQuery("select 1");
            cpds.setIdleConnectionTestPeriod(1000);
        } catch (PropertyVetoException e) {
            log.error("init c3p0 DataSource error!", e);
        }
    }

    public static ComboPooledDataSource getDataSource(){
        return cpds;
    }

    public static Object getObject(String sql) throws SQLException {
        QueryRunner run = new QueryRunner(DBUtils.getDataSource());
        ResultSetHandler<Object> h = new ResultSetHandler<Object>() {
            public Object handle(ResultSet rs) throws SQLException {
                if (!rs.next()) {
                    return null;
                }
                return rs.getObject(1);
            }
        };

        Object result = run.query(sql, h);
        return result;
    }


    public static List getObjects(String sql, Class clazz) throws SQLException {
        QueryRunner run = new QueryRunner(DBUtils.getDataSource());
        ResultSetHandler<List> h = new BeanListHandler(clazz);
        List list = run.query(sql, h);
        return list;
    }


}
