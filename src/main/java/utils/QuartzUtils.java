package utils;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.UUID;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Created by peter on 16/7/7.
 */
public class QuartzUtils {

    public static SchedulerFactory sf = new StdSchedulerFactory();


    public static void scheduleJob(Class clazz, String cronExpression) throws SchedulerException {
        Scheduler sched = null;
        try {
            sched = sf.getScheduler();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        String uuid = UUID.randomUUID().toString();

        JobDetail job = newJob(clazz)
                .withIdentity("job_" + uuid, "group_" + uuid)
                .build();

        CronTrigger trigger = newTrigger()
                .withIdentity("trigger_" + uuid, "group_"+ uuid)
                .withSchedule(cronSchedule(cronExpression))
                .build();

        sched.scheduleJob(job, trigger);

        sched.start();
    }
}
