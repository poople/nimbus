import job.LiveShowJob;
import job.MessageProduceJob;
import job.UpdateTotalNumberJob;
import lombok.extern.slf4j.Slf4j;
import org.quartz.SchedulerException;
import utils.ConfigUtil;
import utils.QuartzUtils;

import static spark.Spark.get;
import static spark.Spark.post;

/**
 * Created by peter on 16/5/28.
 */
@Slf4j
public class HammerMessageProducer {

    public static void main(String[] args) {
//        try {
//            QuartzUtils.scheduleJob(MessageProduceJob.class, ConfigUtil.getString("messageProduce.cronExpression"));
//            QuartzUtils.scheduleJob(UpdateTotalNumberJob.class, ConfigUtil.getString("updateTotalNumber.cronExpression"));
//        } catch (SchedulerException e) {
//            log.info("### schedule job error!", e);
//        }

        startReloadConfigServer();

        log.info("####### Server started!");
    }

    private static void startReloadConfigServer() {
        log.info("### start reload config server");
        post("/config/reload", (req, res) -> {
            ConfigUtil.loadConfig();
            return "reload config success!";
        });

        get("/config/:key", (request, response) -> {
            String key = request.params(":key");
            String value = ConfigUtil.getString(key);
            return "key=" + key + ", value=" + value;
        });

        post("/schedule/job", (req, res) -> {
            String params = req.body();
            log.info("request body: {}", params );
            LiveShowJob.start(params);
            return "success!";
        });
    }


}

