package job;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import utils.ConfigUtil;
import utils.HttpUtils;
import utils.KafkaUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by peter on 2017/2/18.
 */
@Slf4j
public class LiveShowJob {

    public static void start(String params){
        String jobId = UUID.randomUUID().toString();
        log.info("starting LiveShowJob {}. params={}", jobId, params);

        List<String> onlineClients = null;
        try {
            log.info("job {} getting online clients.", jobId);
            onlineClients = getOnlineClients();
            log.info("job {} onlineClients size={}", jobId, onlineClients==null?0: onlineClients.size());
        } catch (IOException e) {
            log.info("get online client error!", e);
        }

        Map<String,Object> paramsMap = new Gson().fromJson(params, Map.class);
        int clientNum = (int) Double.parseDouble(String.valueOf(paramsMap.get("client_num")));

        if(onlineClients != null && onlineClients.size() > 0){
            int taskSize = clientNum > onlineClients.size() ? onlineClients.size(): clientNum;
            log.info("job {}. client_num={}, onlineClients={}, taskSize={}", jobId, clientNum, onlineClients.size(), taskSize);
            String topic = ConfigUtil.getString("message.topic");

            if(taskSize > 0) {
                for (int i = 0; i < taskSize ; i++) {
                    paramsMap.put("onlyId", getOnlyId(onlineClients, i));
                    String key = UUID.randomUUID().toString();
                    String value = getMessage(paramsMap);
                    KafkaUtils.send(topic, key, value);
                    log.info("job {} sent message: topic={}, key={}, value={}", jobId, topic, key, value);
                }
            }else{
                log.info("schedule number is zero.");
            }
        }else{
            log.info("online client is empty");
        }

    }

    private static String getOnlyId(List<String> onlineClients, int i){
        int j = i;
        if(i >= onlineClients.size()){
            j =  i - onlineClients.size();
        }
        return onlineClients.get(j);
    }

    private static String getMessage(Map<String,Object> paramsMap){
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("tv_name", paramsMap.get("tv_name"));
        messageMap.put("tv_url", paramsMap.get("tv_url"));
        //messageMap.put("onlyId", paramsMap.get("onlyId"));
        MessageBean message = new MessageBean();
        message.setMessage(messageMap);
        message.setOnlyId((String) paramsMap.get("onlyId"));

        String result = gson.toJson(message);
        return result;
    }

    private static List<String> getOnlineClients() throws IOException {
        String url = ConfigUtil.getString("hammer.onlineClient.url") +  "/type/flash";
        String sysMode = ConfigUtil.getString("hammer.sysMode");
        if(sysMode.equals("--debug")){
            url += "?debug=1";
        }
        Map content = HttpUtils.getJson(url, Map.class);
        List<String> list = (List<String>) content.get("data");
        return list;
    }

    @Data
    private static class MessageBean{
        String onlyId;
        Map<String, Object> message;
    }

}
