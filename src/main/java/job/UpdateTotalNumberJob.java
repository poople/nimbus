package job;

import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import utils.CommonUtils;
import utils.CurveUtils;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by peter on 16/7/7.
 */
@DisallowConcurrentExecution
@Slf4j
public class UpdateTotalNumberJob implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("### start UpdateTotalNumberJob");
        try {
            int before = CurveUtils.getTotal();
            int total = CurveUtils.getTotalNumber(jobExecutionContext.getFireTime().getTime());
            CurveUtils.setTotal(total);
            log.info("### update total number. before {}, after {}", before, total);

            long expireTime = computeExpireTime(jobExecutionContext.getFireTime());
            CommonUtils.setExpireTime(expireTime);
            log.info("### update expireTime to {}", expireTime);
        } catch (SQLException e) {
            log.error("", e);
        }
    }

    private long computeExpireTime(Date now){
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        cal.add(Calendar.DAY_OF_YEAR, 1);
        long expireUnixTime = cal.getTimeInMillis() / 1000 - 1;

        return expireUnixTime;
    }

}
