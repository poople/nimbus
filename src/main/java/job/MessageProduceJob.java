package job;

import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import service.NimbusService;
import utils.*;

import java.io.IOException;
import java.util.*;

/**
 * 消息生产者。
 * 以固定频率，按照指定的消息大小和格式，将消息写入生产队列中。
 *
 */
@Slf4j
@DisallowConcurrentExecution
public class MessageProduceJob implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("### start MessageProduceJob");
        String taskName = CommonUtils.HAMMER_TASK.getName();
        NimbusService nimbusService = new NimbusService();

        List<String> onlineClients = null;
        try {
            onlineClients = getOnlineClients();
        } catch (IOException e) {
            log.info("### get online client error!", e);
        }

        if(onlineClients != null){
            log.info("### Starting shuffle clients");
            Collections.shuffle(onlineClients);

            int currMinTaskNumber = CurveUtils.getTaskNumber(CurveUtils.getTotal(), CurveUtils.getTotalRest());
            CurveUtils.set(currMinTaskNumber);
            log.info("### Setting current minute task number {}, total={},rest={}", currMinTaskNumber, CurveUtils.getTotal(), CurveUtils.getTotalRest());

            log.info("#### onlineClients length: " + onlineClients.size());
            for (String clientOnlyId : onlineClients) {
                try {
                    log.info("### try to produce MESSAGE for client:" + clientOnlyId);
                    if (CurveUtils.get() > 0) {
                        String topic = ConfigUtil.getString("message.topic");
                        String key = UUID.randomUUID().toString();
                        String value = nimbusService.getMessage(clientOnlyId);

                        if (value == null) {
                            continue;
                        }

                        Boolean dryRunMode = ConfigUtil.getBoolean("nimbus.dryRun.mode");
                        if (!dryRunMode) {
                            KafkaUtils.send(topic, key, value);
                        }

                        CurveUtils.decrementAndGet();
                        incrTimes(clientOnlyId, taskName);
                        updateLastSendTimes(clientOnlyId);

                        log.info(" Sent message: topic={}, key={}, value={}", topic, key, value);
                    }
                } catch (Exception e) {
                    log.error("发送消息出错", e);
                }
            }
        }
    }

    private List<String> getOnlineClients() throws IOException {
        String url = ConfigUtil.getString("hammer.onlineClient.url");
        String sysMode = ConfigUtil.getString("hammer.sysMode");
        if(sysMode.equals("--debug")){
            url += "?debug=1";
        }
        Map content = HttpUtils.getJson(url, Map.class);
        List<String> list = (List<String>) content.get("data");
        return list;
    }

    private static void incrTimes(String key, String sysTask) {
        String countKey = RedisUtils.keyWithCount(key, sysTask);
        RedisUtils.incr(countKey);
    }

    private static void updateLastSendTimes(String key) {
        String sendTimeKey = RedisUtils.keyWithLastSendTime(key);
        RedisUtils.setLongValue(sendTimeKey, (new Date()).getTime());
    }
}