package bean;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 发送的消息体格式
 *
 {
 "onlyId": "xxx", // pc唯一id
 "message": {
 "target": "www.hao123.com/?tn={params}", //任务目标地址，包含参数配置
 "sysTask": "bd", //执行任务类型
 "maxHit": 3, //单次下发，phantom执行任务次数
 "args": {
 "keywords": "github",
 "yAxis": 760
 }, //任务执行中需要配置的参数，包括查询关键词等
 "query": [{
 "mask": "params", //任务目标地址，需要替换的mask
 "vals": ["test1", "test2"] // 对应目标地址中的实参选项
 }],
 "ua": "Mozilla/5.0 (Window NT 6.1; WOW64) AppleWebKit/538.1 (KHTML, like Gecko) Chrome/43.0.2357 Safari/538.1",
 "cookies": "[{\"name\":\"name001\", \"value\":\"value001\", \"domain\":\"domain001\", \"path\":\"path001\", \"httponly\":false, \"secure\":false, \"expire\":1439301878294} .... { ... }]", //cookie字符串，初始化phantom需要
 "plugins": "[{}, ..., {}]" // plugins字符串，初始化phantom需要
 }
 }

 * Created by peter on 16/6/5.
 */
@Data
public class MessageBean {

    private String onlyId;

    private MessageInfo message = new MessageInfo();

    @Data
    public static class MessageInfo {
        private String target;
        private String sysTask;
        private Integer maxHit;
        private String ua;
        private Map<String, Object> args = new HashMap<>();
        private String cookies;
        private String plugins;
        private List<Map<String, Object>> query = new ArrayList<>();
    }
}
