package filter;

/**
 * Created by peter on 16/7/7.
 */
public interface IFilter {

    boolean filter(NimbusContext context);
}
