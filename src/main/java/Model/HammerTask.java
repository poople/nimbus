package Model;

import lombok.Data;

/**
 * Created by peter on 16/7/13.
 */
@Data
public class HammerTask {
    private long id;
    private String name;
    private String target;
}
