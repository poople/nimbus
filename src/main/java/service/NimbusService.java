package service;

import Model.HammerTask;
import bean.MessageBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import utils.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by peter on 16/7/6.
 */
@Slf4j
public class NimbusService {

    private class userDetail{

    }


    /**
     1、单个客户端24小时内执行任务次数不超过4次
     2、单个客户端连续两次执行任务的间隔至少为15分钟
     3、单个客户端执行的计费名tn号，要尽量分散
     */
    public String getMessage(String clientOnlyId) throws SQLException, IOException {
        String taskName = CommonUtils.HAMMER_TASK.getName();

        if(isOverTimes(clientOnlyId, taskName)){
            log.info("### is over times. key={}", clientOnlyId);
            return null;
        }

        if(isFrequencyTooHigh(clientOnlyId)){
            log.info("### is frequency too high. key={}", clientOnlyId);
            return null;
        }

        MessageBean messageBean = assemblyMessageBean(clientOnlyId);
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String result = gson.toJson(messageBean);
        return result;
    }

    private MessageBean assemblyMessageBean(String clientOnlyId) throws IOException, SQLException {
        int maxHit = ConfigUtil.getInt("hammer.maxHit");
        HammerTask task = CommonUtils.HAMMER_TASK;
        String taskName = getTaskName();
        long taskId = task.getId();
        String tn = getTn(clientOnlyId, taskName);

        MessageBean messageBean = new MessageBean();

        messageBean.setOnlyId(clientOnlyId);

        messageBean.getMessage().setSysTask(taskName);
        messageBean.getMessage().setTarget(task.getTarget());
        messageBean.getMessage().setMaxHit(maxHit);

        Map<String, Object> map = new HashMap<>();
        map.put("vals", tn);
        map.put("mask", "param");
        messageBean.getMessage().getQuery().add(map);

        Map<String, String> userDetailInfo = getUserDetail(clientOnlyId, taskId);
        String plugins = userDetailInfo.get("plugins");
        String cookies = userDetailInfo.get("cookies");
        String ua = userDetailInfo.get("ua");
        String hotword = userDetailInfo.get("hotword");

        messageBean.getMessage().setPlugins(plugins);
        messageBean.getMessage().setCookies(cookies);
        messageBean.getMessage().getArgs().put("keywords", hotword);

        messageBean.getMessage().setUa(ua);
        return messageBean;
    }

    private Map<String, String> getUserDetail(String onlyId, long taskId) throws IOException {
        String url = ConfigUtil.getString("hammer.userDetail.url").replace("{onlyId}", onlyId).replace("{taskId}", String.valueOf(taskId));
        log.info("request url: {}", url);
        Map content = HttpUtils.getJson(url, Map.class);

        String cookies = new Gson().toJson(((Map)content.get("data")).get("cookies"));
        String hotword = (String) ((Map) ((Map)content.get("data")).get("hotword")).get("query_word");
        String ua = (String) ((Map)content.get("data")).get("ua");
        String plugins = new Gson().toJson(((Map)content.get("data")).get("plugins"));


        Map<String, String> result = new HashMap<>();
        result.put("plugins", plugins);
        result.put("cookies", cookies);
        result.put("ua", ua);
        result.put("hotword", hotword);

        return result;
    }

    private String getTn(String key, String taskType){
        String tnKey = key+ "_" +taskType+"_tns";
        Set<String> tns = getTns();

        if(RedisUtils.llen(tnKey) == 0){
            for(String tn: tns) {
                RedisUtils.lpush(tnKey, tn);
            }
        }

        String tn = RedisUtils.rpop(tnKey);
        RedisUtils.lpush(tnKey, tn);

        return tn;
    }

    private Set<String> getTns(){
        String tns = ConfigUtil.getString("hammer.tns");
        Set<String> result = new HashSet<>();
        for(String tn: tns.split(",")){
            result.add(tn);
        }
        return result;
    }

    private boolean isOverTimes(String key, String taskType){
        int maxTimes = ConfigUtil.getInt("message.maxTimes");
        String countKey = RedisUtils.keyWithCount(key, taskType);
        int times = RedisUtils.getValueInt(countKey);
        if(times >= maxTimes){
            return true;
        }else{
            return false;
        }
    }

    private boolean isFrequencyTooHigh(String key){
        long now = (new Date()).getTime();
        String lastSendTimeKey = RedisUtils.keyWithLastSendTime(key);
        long lastSendTime = RedisUtils.getValueLong(lastSendTimeKey);
        lastSendTime = lastSendTime + ConfigUtil.getInt("message.duration.milliseconds");

        if(now <= lastSendTime){
            return true;
        }else{
            return false;
        }
    }

    private String getTaskName(){
        float randomValue = new Random().nextFloat();
        if(randomValue <= 0.035){
            HammerTask task = CommonUtils.HAMMER_TASK;
            String taskName = task.getName();
            return taskName;
        }else {
            return "common";
        }
    }


}
