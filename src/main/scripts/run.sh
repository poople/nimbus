#!/bin/bash

base_dir=$(dirname $0)/..

CLASSPATH=$CLASSPATH:"$base_dir"/lib/*
CONF_FILE="$base_dir"/conf/server.properties
LOG_FILE="$base_dir"/conf/logback.xml
MAIN_CLASS=HammerMessageProducer

# Log directory to use
if [ "x$LOG_DIR" = "x" ]; then
    LOG_DIR="$base_dir/logs"
fi
# create logs directory
if [ ! -d "$LOG_DIR" ]; then
    mkdir -p "$LOG_DIR"
fi


LOG_OPTS="-Dlogback.configurationFile=$LOG_FILE -Dlog.dir=$LOG_DIR"
CONF_FILE_OPTS="-Dconf.file=$CONF_FILE"
JVM_OPTS="-Xmx256m -Xms256m -server -XX:+UseG1GC"

echo "##### Starting server"
nohup java $JVM_OPTS $CONF_FILE_OPTS $LOG_OPTS -cp $CLASSPATH $MAIN_CLASS >/dev/null 2>&1 &
